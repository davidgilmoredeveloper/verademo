image: ricardoapjunior/curl-unzip-java-maven-git

variables:
  OUTPUT_FILE: "app/target/verademo.war"
  BASELINE_FILE: "pipeline_baseline.json"
  PROJECT_NAME: "Verademo-Gitlab"
  UPDATE_BASELINE_COMMIT_MESSAGE: "Updating baseline on development branch during CI process"
  REPOSITORY_NAME: "verademo"

  SHOULD_RUN_ON_DEVELOP: $CI_COMMIT_BRANCH == "develop" && $CI_COMMIT_MESSAGE !~ $UPDATE_BASELINE_COMMIT_MESSAGE && $CI_PIPELINE_SOURCE != "merge_request_event"
  SHOULD_RUN_ON_MASTER: $CI_PIPELINE_SOURCE == "schedule"
  SHOULD_RUN_ON_FEATURE_BRANCH: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" && $CI_COMMIT_BRANCH != "develop"

stages:
  - sca
  - build
  - security
  - commit-baseline-file

#runs on all executions of the pipeline. The pipeline is only called on merge request events, on schedule or on develop (after a merge)
.run-for-all-calls: &run-for-all-calls
  rules:
    - if: '$SHOULD_RUN_ON_FEATURE_BRANCH'
      when: always
    - if: '$SHOULD_RUN_ON_MASTER'
      when: always
    - if: '$SHOULD_RUN_ON_DEVELOP'
      when: always
    - when: never

#runs on commits to develop, as develop is protected, this will only happen when a merge-request is merged
.run-after-merge: &run-after-merge
  rules:
    - if: '$SHOULD_RUN_ON_DEVELOP'
      when: always
    - when: never

#runs only on scheduled jobs on master
.run-on-schedule: &run-on-schedule
  rules:
    - if: '$SHOULD_RUN_ON_MASTER'
      when: always
    - when: never

#runs on merge events on feature branches
.run-on-merge-request-event: &run-on-merge-request-event
  rules:
    - if: '$SHOULD_RUN_ON_FEATURE_BRANCH'
      when: always
    - when: never

sca-scan:
  <<: *run-for-all-calls
  stage: sca
  script:
    - echo "Starting Software Composition Analysis"
    - curl -sSL https://download.sourceclear.com/ci.sh | sh

build-app:
  <<: *run-for-all-calls
  stage: build
  artifacts:
    paths:
      - $OUTPUT_FILE
    expire_in: 1 day
  script:
    - mvn -f app/pom.xml clean package
    - chmod +x $OUTPUT_FILE

pipeline-scan:
  <<: *run-on-merge-request-event
  stage: security
  artifacts:
    reports: 
      sast: veracode_gitlab_vulnerabilities.json
    name: ${CI_PROJECT_NAME}_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}_pipeline-results
    paths:
      - results.json
      - veracode_gitlab_vulnerabilities.json
      - results.txt
    expire_in: 1 week
  script:
    - echo "Starting Pipeline Scan"
    - curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip
    - unzip pipeline-scan-LATEST.zip pipeline-scan.jar
    - java -jar pipeline-scan.jar
      --veracode_api_id "${VERACODE_API_ID}"
      --veracode_api_key "${VERACODE_API_KEY}"
      --file $OUTPUT_FILE
      --project_name "${PROJECT_NAME}"
      --baseline_file "baseline.json"
      --project_name "${CI_PROJECT_PATH}"
      --project_url "${CI_REPOSITORY_URL}"
      --project_ref "${CI_COMMIT_REF_NAME}"
      --gl_vulnerability_generation="true"
      --issue_details "true"
      --summary_output "true"

sandbox-scan:
  <<: *run-after-merge
  stage: security
  image: veracode/api-wrapper-java:latest
  script:
    - echo "Starting Sandbox Scan"
    - java -jar /opt/veracode/api-wrapper.jar
      -vid ${VERACODE_API_ID}
      -vkey ${VERACODE_API_KEY}
      -action UploadAndScan
      -appname "${PROJECT_NAME}"
      -version $CI_JOB_ID
      -sandboxname "Verademo-Sandbox-Gitlab"
      -filepath $OUTPUT_FILE
      -createprofile false

make-new-baseline:
  <<: *run-after-merge
  stage: security
  artifacts:
    paths:
      - $BASELINE_FILE
    expire_in: 1 day
  script:
    - echo "Setting new baseline file for develop branch"
    - curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip
    - unzip pipeline-scan-LATEST.zip pipeline-scan.jar
    - java -jar pipeline-scan.jar
      --veracode_api_id "${VERACODE_API_ID}"
      --veracode_api_key "${VERACODE_API_KEY}"
      --file $OUTPUT_FILE
      --project_name "${PROJECT_NAME}"
      --json_output_file "${BASELINE_FILE}"
      --fail_on_severity ""

#commit-baseline-file:
#  <<: *run-after-merge
#  stage: commit-baseline-file
#  script:
#    - echo "Setting up git respository for execution"
#    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
#    - eval $(ssh-agent -s)
#    - echo "$GITLAB_SSH_KEY" | tr -d '\r' | ssh-add - > /dev/null
#    - mkdir -p ~/.ssh
#    - chmod 700 ~/.ssh
#    - echo "$GITLAB_SSH_KEY" >> ~/.ssh/id_rsa.pub
#    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'


#    - echo "Cloning repository"
#    - git clone git@gitlab.com/davidgilmoredeveloper/${REPOSITORY_NAME}.git
#    - mv ${BASELINE_FILE} ./${REPOSITORY_NAME}
#    - cd ./${REPOSITORY_NAME}

#    - git config user.email "dgilmore@veracode.com"
#    - git config user.name "David Gilmore"

#    - git add ${BASELINE_FILE}
#    - git commit -m "${UPDATE_BASELINE_COMMIT_MESSAGE}"
#    - git push origin HEAD:$CI_COMMIT_REF_NAME

policy-scan:
  <<: *run-on-schedule
  stage: security
  image: veracode/api-wrapper-java:latest
  script:
    - echo "Starting Policy Scan"
    - java -jar /opt/veracode/api-wrapper.jar
      -vid ${VERACODE_API_ID}
      -vkey ${VERACODE_API_KEY}
      -action UploadAndScan
      -appname "${PROJECT_NAME}"
      -version $CI_JOB_ID
      -filepath $OUTPUT_FILE
      -createprofile false